module Utilities

  # Check for a valid internet connection
  #=> Boolean
  def self.internet?
    begin
      true if open("http://www.google.com/")
    rescue
      false
    end
  end

  # attempt_dropbox - Attempt to create a Dropbox client
  #=> DropboxUserClient || nil
  def self.attempt_dropbox(internet = false)
    root    = "#{File.dirname(File.dirname(File.dirname(__FILE__)))}"
    api_dir = "#{root}/lib/assets/dropbox"
    if File.directory?(api_dir) && internet
      @@dropbox = DropboxHelper::DropboxUserClient.new(api_dir)
    else
      @@dropbox = nil
    end
  end

  # refresh_connections - Attempt to re-establish connections
  #=> Hash
  def self.refresh_connections
    connections = {}
    connections.store(:internet, self.internet?) 
    connections.store(:dropbox, self.attempt_dropbox)
    return connections
  end

  # most_recent - Get the most recently edited file in a directory
  #=> String
  def self.most_recent(dir)
    recent = `find #{dir} -type f -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" "`.chomp
    File.basename(recent)
  end

  # common_files - Get common files between two arrays of Userfile Hashes
  #=> Sorted Array of UserFile
  def self.common_files(arr1, arr2)
    ret = []
    flattened_arr2 = []
    flattened_arr2 << arr2.map { |f| f.metadata }.pop
    flattened_arr2.map!{ |k| k[:basename] }
    arr1.each do |f|
      puts f.basename
      ret << f if flattened_arr2.include?(f.basename)
    end
    ret.sort_by { |f| f.modified[:mtime] }.reverse!
  end

end # End module Utilities