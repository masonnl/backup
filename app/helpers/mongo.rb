require 'mongo'

module MongoHelper

  class MongoClient
    attr_accessor :client
    def initialize(credentials)
      @client   = Mongo::Client.new(credentials)
      Mongo::Logger.logger.level = Logger::INFO
      @backups  = @client[:backups]
    end

    # exists? - Tell whether or not a document can be found in the db
    #=> Boolean
    def exists?(file)
      file = @backups.find({"file"=> file}).to_a
      file.empty? ? false : true
    end

    # get - Pull a document based on filename
    #=> Hash || nil
    def get(file)
      @backups.find({"file"=> file}).to_a[0]
    end

    # get_list - Pull all documents in the db for analysis
    #=> Array of Hash
    def get_list
      @backups.find.to_a
    end

    # get_files - Pull an array of file names stored in the db
    #=> Array
    def get_files
      files = []
      get_list.each{ |doc| files << doc["file"] }
      files
    end

    # insert - Wrap the mongo insert_one method
    #=? Response
    def insert(doc)
      response = @backups.insert_one(doc)
      response.n
    end

    # destroy - Destroy a document in the database
    #=> Response
    def destroy(doc)
      response = @backups.find_one_and_delete(doc)
    end

    #insert_file - Wrap the mongo Grid fs insert
    #=> Response
    # TODO:: Allow backup of LARGE files (100mb+)
    def insert_file(file)
      @client.database.fs.insert_one(file)
    end
  end # end class MongoClient

end # end module MongoHelper