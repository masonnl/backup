require 'json'

module FileHelper

  # UserFile - Objectify a User's File to make accessing that file trivial
  class UserFile
    attr_accessor :modified, :metadata
    attr_reader :dropbox, :basename, :fullpath
    def initialize(name, dropbox = false)
      @dropbox  = dropbox
      @fullpath = name
      @basename = File.basename(name)
      @modified = modified_datetime
      @metadata = metadata
    end

    def modified_datetime
      if !@dropbox
        mtime = File.mtime(@fullpath).to_s
        { :mtime => mtime,
          :date  => `echo #{mtime} | awk '{print $1}'`,
          :time  => `echo #{mtime} | awk '{print $2}'`}
      else
        {}
      end
    end

    def metadata
      if !@dropbox
        { :fullpath => @fullpath,
          :basename => @basename,
          :modified => @modified }
      else
        {}
      end
    end

    def to_json
      JSON.generate(@metadata)
    end
  end # End class UserFile


  # Files - A data structure to store UserFile objects in a hash
  #=> Hash: {'full_path' => UserFile}
  class Files
    attr_accessor :files
    def initialize
      @files = Hash.new
    end

    # Create and add a UserFile to the hash if it doesn't exist already
    #=> Void
    def add(path)
      unless @files.include?(path)
        @files.store(path, UserFile.new(path))
      end
    end

    # Returns the size of the hash
    #=> Int
    def size
      @files.size
    end

    # sort_files - Takes a hash of file objects and orders them by value
    #=> Array
    def sort_files
      @files.each_value.sort_by { |f| f.modified[:mtime] }.reverse!
    end
  end # End class Files

end # End module FileHelper
