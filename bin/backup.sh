#!/bin/bash

################################################################################
### This backup script will automate the process of creating, encrypting, and
### storing backups. Whether locally or remotely, this is your tool.

### NOTE: This may be a very crude implementation. I am by no means a BASH
### expert and any pull requests explaining different implementations
### would be welcome!
################################################################################

if [ "$(whoami)" == "root" ]; then
	read -p "This script is being run as root! Any files generated under the root \
user will be owned by the root user! Is that okay? (Y/n) " -r
	if [[ ! $REPLY =~ ^[Yy]$ ]]; then
		echo "Aborting..." && exit 0
	fi
fi

# Set script directories and timestamp
args=("$@")
script=$( basename ${BASH_SOURCE[0]} )
bin=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
base=$( dirname $bin ) && cd $base
datetime=$(date +%Y-%m-%d-%H-%M-%S)
backup_dir=$base/backups
backup_log=$base/logs/backup/backup.log
if [ -f $backup_log ]; then
	rm $backup_log && touch $backup_log
else
	touch $backup_log
fi

# Set ownership of the backup_log dir to owner of this script
chown -R $(stat -c '%U' $(dirname "${BASH_SOURCE[0]}")) $backup_log

# Set fonts for display_usage
NORM=$( tput sgr0 )
BOLD=$( tput bold )
REV=$( tput smso )

## display_usage - Display the usage for this backup script
function display_usage() {
	echo -e "${BOLD}NAME${NORM}"
	echo -e "\t${BOLD}backup - Open source application for backing up files to the cloud${NORM}"
	echo -e \\n"${BOLD}DESCRIPTION${NORM}"
	echo -e "This backup script will simplify the process of backing up user files \n\
and automate the process of sending them to cloud storage hosts." | sed 's/^/\t/'
	echo -e \\n"${BOLD}SYNOPSIS${NORM}"
	echo -e "\t${BOLD}$script [FLAGS]${NORM}"
	echo -e \\n"${BOLD}FLAGS${NORM}"
	echo -e "\t${BOLD}-b, --backup${NORM}\t Provides a path to a file to backup"
	echo -e "\t${BOLD}-h, --help${NORM}\t You're looking at it"
	exit 0
}

## log - Log a string to the $backup_log
# $@ - All args will act as a string to log into $backup_log
function log() {
	# Should not pass a 0-length string
	if [[ $# > 0 ]]; then
		echo "$@" && echo "$@" >> $backup_log
	fi
}

## backup - Spawn a backup of a specific file or directory
# $1 is the target backup
function backup() {
	target="$@"
	# No args were passed
	if [[ $# == 0 ]]; then
		msg="[ERROR] Must pass an existing file to be backed up"
		log $msg && exit 1
	# Directory || file exists
	elif [ -d "$target" ] || [ -f "$target" ]; then
		# Set up pathing
		dir_path=$( dirname "$target" )
		path_actual=$( basename "$target" )
		cd $dir_path

		# Set up a history.log file for convenience
		history_log=history.log
		touch $history_log
		if [ -d "$target" ]; then
			# because it is a directory we need a history of its path & contents
			tree -a "$dir_path/$path_actual" > $history_log
		elif [ -f "$target" ]; then
			echo "$dir_path/$path_actual" > $history_log
		fi

		## Commence backup...
		log "[BACKUP] -----[ Initiating backup on: ]-----"
		log "[BACKUP] Beginning tarball process on: $dir_path/$path_actual"
		tar -zcpf $backup_dir/$datetime.tar.gz --exclude=$backup_dir/$datetime.tar.gz \
			"$path_actual" $history_log | sed 's/^/[BACKUP]    /' | tee -a $backup_log
		log "[BACKUP] Completed tarball: $backup_dir/$datetime.tar.gz @ \
$( du -h --max-depth=0 $backup_dir/$datetime.tar.gz | awk '{print $1}' )"
		
		# encrypt data if secrets are found
		if [ -e "$base/lib/.secret" ]; then
			encrypt $backup_dir/$datetime.tar.gz
			rm $backup_dir/$datetime.tar.gz
		fi

		# Cleanup...
		rm $history_log
		msg="[BACKUP] Complete on $( date ) and stored in $backup_dir"
		log $msg && sed -i -e "s/^/[$datetime]/" $backup_log && exit 0
	else
		msg="[ERROR] '$target' is not a valid file."
		log $msg && exit 1
	fi
}

## encrypt - Encrypt a file.tar.gz
# $1 - File to encrypt (ie. path/to/file)
function encrypt() {
	cwd=$(pwd)
	# No args were passed
	if [[ $# == 0 ]]; then
		log "[ERROR] You must specify a file to encrypt"
	else
		# Set up pathing
		full_path=$( readlink -f $1 )
		cd $( dirname $full_path )

		# Check for a valid tarball
		if tar tf $full_path &> /dev/null; then
			# Encrypt...
			msg="[ENCRYPT] $full_path is a valid tarball. Encrypting..."
			openssl aes-256-cbc -a < $full_path > $1.enc -pass file:"$base/lib/.secret"
			log $msg && sed -i -e "s/^/[$datetime]/" $backup_log
		else
			log "[ERROR] $full_path is not a valid tarball"
		fi
		cd $cwd
	fi
}

## main - Execute a backup on a given file
function main() {
	# Error if no flags are given
	if [[ ${#args[*]} == 0 ]]; then
		echo -e "No flags were given!\n" && display_usage
	fi

	###  Run getopt  ###
	MAIN=$( getopt -o b:h:: -l "backup:,help::" -n '$script' -- "$@" )
	eval set - "$MAIN"
	path=$( echo "$2 ${*:4}" | sed -e 's/ *$//' )
	while true; do
		case $1 in 
			-b|--backup) backup "$path" ;;
			-h|--help) display_usage ;;
			-) break ;;
			*) echo "Unhandled option $arg" ;;
			?) display_usage ;;
		esac
	done
}

## Main - Spin up and backup
main $@