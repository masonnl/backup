#!/bin/bash

################################################################################
### This recovery script will automate the process of retrieving and unpacking
### priorly created backups created by the backup application. It will also
### offer simple decryption/unpacking procedures.

### NOTE: This may be a very crude implementation. I am by no means a BASH
### expert and any pull requests explaining different implementations
### would be welcome!
################################################################################

# Set script directories and timestamp
args=("$@")
script=$( basename ${BASH_SOURCE[0]} )
bin=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
base=$( dirname $bin ); cd $base
datetime=$(date +%Y-%m-%d-%H-%M-%S)
recovery_dir=$base/recoveries
temp_dir=$recovery_dir/temp; mkdir -p $temp_dir
recovery_log=$base/logs/restore/recovery.log
if [ -f $recovery_log ]; then
	rm $recovery_log; touch $recovery_log
else
	touch $recovery_log
fi

# Set ownership of the recovery_log dir to owner of this script
chown -R $(stat -c '%U' $(dirname "${BASH_SOURCE[0]}")) $recovery_log

# Set fonts for display_usage
NORM=$( tput sgr0 )
BOLD=$( tput bold )
REV=$( tput smso )

## display_usage - Display the usage for this recovery script
function display_usage() {
	echo -e "${BOLD}NAME${NORM}"
	echo -e "${BOLD}recover - Open source application for recovering files that were priorly \n\
backed up to the cloud${NORM}" | sed 's/^/\t/'
	echo -e \\n"${BOLD}DESCRIPTION${NORM}"
	echo -e "This recovery script will simplify the process of retrieving user files \n\
and automate the process of decrypting and restoring them to their original locations." | sed 's/^/\t/'
	echo -e \\n"${BOLD}SYNOPSIS${NORM}"
	echo -e "\t${BOLD}$script [FLAGS]${NORM}"
	echo -e \\n"${BOLD}FLAGS${NORM}"
	echo -e "\t${BOLD}-d, --decrypt${NORM}\t Decrypt a file"
	echo -e "\t${BOLD}-h, --help${NORM}\t You're looking at it"
	echo -e "\t${BOLD}-r, --restore${NORM}\t Restore a backup into the original directory it was backed up in"
	echo -e "\t${BOLD}-u, --unpack${NORM}\t Unpack a tarball"
	exit 0
}

## restore - Once a file has been decrypted:
function restore() {
	cd $temp_dir
	if [ ${1: -3} == enc ]; then
		echo "Restore beginning on: $1" >> $recovery_log
		decrypt $1; unpack ${1%.enc}
	else
		unpack $1
	fi
	exit 0
}

## decrypt - Decrypt a file.enc -> file.tar.gz
function decrypt() {
	target="$(basename ${1%.enc})"
	openssl aes-256-cbc -d -a < $1 > "$target" -pass file:"$base/lib/.secret"
}

## unpack - Unpack the contents of a tarball
# $1 - a tarball (possibly containing a history.log)
function unpack() {
	tar -zxpf "$(basename $1)"
	if [ -f history.log ]; then
		full_path=$(head -n 1 history.log | sed 's/ //g')
		recovery_dir=$(dirname $full_path)
		rm history.log && rm $(basename $1)
		mkdir -p $recovery_dir
		cp -r . $recovery_dir
		cd $base/recoveries && rm -rf $temp_dir
	fi
}

## main - Execute a recover on a given file
function main() {
	# Error if no flags are given
	if [[ ${#args[*]} == 0 ]]; then
		echo -e "No flags were given!\n" && display_usage
	fi

	###  Run getopt  ###
	MAIN=$( getopt -o d:h::r:u: -l "decrypt:,help::,restore:,unpack:" -n '$script' -- "$@" )
	eval set -- "$MAIN"
	while true; do
		case $1 in
			-d|--decrypt) shift && decrypt $1 ;;
			-h|--help) display_usage ;;
			-r|--restore) shift && restore $1 ;;
			-u|--unpack) shift && unpack $1 ;;
		esac
	done
}
main $@