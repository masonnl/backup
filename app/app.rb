require 'sinatra'
require 'haml'
require 'json'

# Require helpers
Dir[File.dirname(__FILE__) + '/helpers/*.rb'].each { |file| require file }

## Backup - Sinatra app to manage a user's backups + restores
class Backup < Sinatra::Base
  helpers DropboxHelper
  helpers FileHelper
  # helpers MongoHelper
  helpers Utilities

  #############################################################################
  ## Setup Backup

  set :port, 10322
  set :raise_errors, true
  set :show_exceptions, true

  # root - The root of the app directory
  @@root    = File.dirname(File.dirname(__FILE__))
  @@api_dir = "#{@@root}/lib/assets/dropbox"

  # mongo - Connect to a local mongodb
  # db    = 'backupRegime'
  # port  = '27017'
  # @@mongo = MongoHelper::MongoClient.new("mongodb://127.0.0.1:#{port}/#{db}")

  # internet - Check for an internet connection
  @@internet = Utilities::internet?

  # dropbox - Create a Dropbox client if cradentials are present
  @@dropbox = Utilities::attempt_dropbox(@@internet)

  #############################################################################
  ## GET

  get '/' do
    @backups = FileHelper::Files.new

    # Create the UserFiles that will be displayed
    Dir.glob("#{@@root}/backups/*") do |file|
      @backups.add(file)
    end

    # Check for an encryption password
    secret = "#{@@root}/lib/.secret"
    if !File.file?(secret) || File.zero?(secret)
      FileUtils.touch(secret)
      @pass = nil
    else
      @pass = open(secret).read
    end

    # Test outbound connections
    if !@@internet || @@dropbox.nil?
      connections = Utilities::refresh_connections 
      @client     = connections[:dropbox]
      @internet   = connections[:internet]
    else
      @client   = @@dropbox
      @internet = @@internet
    end

    # Compare locally owned backups against those in the cloud
    if @@internet && !@@dropbox.nil?
      @dropbox_files  = @@dropbox.get_files #=> [UserFile]
    else
      @dropbox_files  = []
    end

    # Order the backups (most recent at top)
    @ordered = @backups.sort_files  #=> [UserFile]
    # @shared_files = Utilities::common_files(@dropbox_files, @ordered)
    haml :index
  end

  get '/browse/?*?' do |path|
    @files = FileHelper::Files.new
    if path == "" || path == nil
      path = "/**"
    else
      path = "/#{path}/**"
    end
    Dir.glob("#{path}") do |file|
      @files.add(file)
    end
    haml :browse
  end

  get '/edit/?*?' do |file|
    # @doc = @@mongo.get(File.basename(file))
    # @exists_in_filesystem = File.file?("@@root/backups/#{File.basename(file)}")
    # @exists_in_dropbox    = @@dropbox.exists?(File.basename(file))
    # @path = @doc["path"]
    haml :edit
  end

  get '/setup' do
    haml :setup
  end

  get '/install' do
    `#{@@root}/bin/setup.sh`
    redirect '/'
  end

  get '/dropbox-signin' do
    @@dropbox = Utilities::attempt_dropbox(@@internet)
    if !@@dropbox.nil?
      redirect '/'
    else
      haml :dropbox_signin
    end
  end

  get '/save/?*?' do |path|
    @file = FileHelper::UserFile.new("/#{path}")
    haml :save
  end

  #############################################################################
  ## POST

  post '/backup' do
    meta    = JSON.parse(params[:meta], {:symbolize_names=>true})
    backup  = meta[:fullpath]
    `#{@@root}/bin/backup.sh "--backup=#{backup}"`
    recent = Utilities.most_recent("#{@@root}/backups")
    backup = "#{@@root}/backups/#{recent}"

    # Set the name of the backup (defaults to DateTime if not given)
    if params.has_key?("name") && !params["name"].gsub(" ", "").empty?
      name = params["name"].gsub(" ", "")
    else
      name = recent
    end

    # Mongoify the backup
      # TODO:: Add support for LARGE files (100Mb+)
      # file = File.open(backup)
      # grid_file = Mongo::Grid::File.new(
      #   file.read,
      #   :filename => name,
      #   :chunk_size => 2048)
      # @@mongo.insert_file(grid_file)
    # Insert the document
      # response = @@mongo.insert({
      #   name:     name,
      #   file:     File.basename(backup),
      #   path:     backup,
      #   modified: File.mtime(backup).to_s })
    redirect '/'
  end

  post '/restore' do
    if params.has_key?("restore_from_local")
      `#{@@root}/bin/recover.sh -r #{params[:restore_from_local]}`
    elsif params.has_key?("download_from_dropbox")
      @@dropbox.dropbox_pull(params[:download_from_dropbox])
    elsif params.has_key?("restore_from_dropbox")
      backup_file = @@dropbox.dropbox_pull(params[:restore_from_dropbox])
      `#{@@root}/bin/recover.sh -r #{backup_file}`
    end
    redirect '/'
  end

  post '/delete' do
    puts "params: #{params}"
    if params.has_key?("delete_local")
      puts "rm #{params["delete_local"]}"
      `rm #{params["delete_local"]}`
    elsif params.has_key?("delete_dropbox")
      @@dropbox.dropbox_destroy(File.basename(params[:delete_dropbox]))
    end
    redirect '/'
  end

  post '/dropbox' do
    if params.has_key?("push")
      @@dropbox.dropbox_push(params[:push])
    elsif params.has_key?("destroy")
      @@dropbox.dropbox_destroy(File.basename(params[:destroy]))
    end
    redirect '/'
  end

  post '/dropbox_signin' do
    app_key     = params[:key]
    app_secret  = params[:secret]
    auth_token  = params[:auth]

    unless File.directory?(@@api_dir)
      FileUtils.mkdir_p(@@api_dir)
    end

    File.open("#{@@api_dir}/.api_key", "w")    { |f| f.write(app_key) }
    File.open("#{@@api_dir}/.app_secret", "w") { |f| f.write(app_secret) }
    File.open("#{@@api_dir}/.auth_token", "w") { |f| f.write(auth_token) }

    @@dropbox = DropboxHelper::DropboxUserClient.new(@@api_dir)
    redirect '/'
  end

  post '/secret' do
    File.open("#{@@root}/lib/.secret", "w") { |f| f.write(params[:pass]) }
    redirect '/'
  end
end	# end class Backup
