#!/bin/bash

###############################################################################
### Setup will simply download + install the tools necessary
### for this application. For a complete list of
### commands run: ./setup -h

### Credit to Linerd from http://tuxtweaks.com/2014/05/bash-getopts/
### for ideas using getopts

### NOTE: This may be a very crude implementation. I am by no means a BASH
### expert and any pull requests explaining different implementations
### would be welcome!
###############################################################################

# Set script base and args
script=$( basename ${BASH_SOURCE[0]} )
bin=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
base=$( dirname $bin )
app=$base/app
args=("$@")

# Set fonts for Help
NORM=$( tput sgr0 )
BOLD=$( tput bold )
REV=$( tput smso )

## display_usage - Display the usage for this setup script
function display_usage() {
	echo -e "${REV}USAGE:${NORM} ${BOLD}$script [FLAGS]"
	echo -e "\tThis script must be run with super-user privileges."
	echo -e \\n"${REV}FLAGS:${NORM}"
	echo -e "\t-h, --help\t\tYou're looking at it"
	echo -e "\t-i, --install\t\tInstall all dependencies"
	exit 0
}

# get_consent - You better be root!
function get_consent() {
	prompt="This script requires root privileges to install scripts... Is that okay? (Y/n): "
	read -p "$prompt" -r
	if [[ $REPLY =~ ^[Yy]$ ]]; then
		exec sudo -- "$bin/$script" "$@"
	else
		display_usage
	fi
}

## switch_user - Choose from a list of users act as
# $* - Command that should be executed as intended user
function switch_user() {
	users=($(cut -d: -f1,3 /etc/passwd | egrep ':[0-9]{4}$' | cut -d: -f1))
	echo -e "\nWhich user should install further packages?"
	echo "NOTE: This user will be executing backups/recoveries."
	echo "Type a number:"
	select user in "${users[@]}"
	do
		case $user in
      *) break;;
		esac
	done

	# Check for user validity & switch
	ret=false
	getent passwd $user >/dev/null 2>&1 && ret=true
	if $ret; then
		echo "Running '$*' as user $user..."
		sudo su -p $user -c $*
	else
		echo "User '$user' does not exist! Exiting..."
		exit 1
	fi
}

## install_packages - Installs dependencies for Python2 and Duplicity
function install_packages() {
	# Setup logging directory
	setup_log=$base/logs/setup/setup.log
	if [ -f $setup_log ]; then
		rm $setup_log && touch $setup_log
	else
		touch $setup_log
	fi

	# Make sure all logs are owned by the owner of this file
	chown -R $(stat -c '%U' $(dirname "${BASH_SOURCE[0]}")) $base/logs/

	# Query for updates and installs
	processes_completed=()

	# Packages to install as root
	if [ "$(whoami)" == "root" ]; then
		apt_packages=(tree bundler) #mongodb-server)
		echo -e "\nPackages to be installed: \n\t${apt_packages[*]}"
		read -p "Is that okay? (Y/n): "
		if [[ $REPLY =~ ^[Yy]$ ]]; then
			echo "apt-get install:"
			echo -e "\n\t###Install packages###" >> $setup_log
			apt-get --assume-yes install ${apt_packages[*]} | tee -a $setup_log
			# Setup mongod
				# mkdir -p /data/db
				# chown mongod /data/db
				# ps axf | grep mongo | grep -v grep | awk '{print "kill -9 " $1}' | sh
				# processes_completed+=("\tinstalled: '${apt_packages[*]}'")
				# mongod &
		else
			echo "Aborting..." >> $setup_log
		fi

		# Echo processes that were completed
		echo -e "\nProcesses completed:\n${processes_completed[*]}"
		echo "Be sure to run this script an a non-root user also!"
	else
		# Check for rvm and bundle install, else redirect to rvm install page
		rvm=`which rvm`
		if [ -e $rvm ]; then
			ruby_packages=($(cat Gemfile | grep gem | awk '{print $2}' | sed "s/'//g" | sed 's/\n//g' | sed '1d'))
			echo -e "\nPackages to be installed: \n\t${ruby_packages[*]}"
			read -p "Is that okay? (Y/n): "
			if [[ $REPLY =~ ^[Yy]$ ]]; then
				printf "bundling...\n"
				bundle install | tee -a $setup_log
				processes_completed+=("\tinstalled: '${ruby_packages[*]}'")
			else
				echo "Aborting..." >> $setup_log
			fi
		else
			echo "Please go to https://rvm.io/rvm/install and follow the instructions for setting up RVM on your machine, then rerun this script!"
			xdg-open https://rvm.io/rvm/install >> /dev/null 2>&1 &
			ruby_packages=()
		fi

		# Echo processes that were completed
		echo -e "\nProcesses completed:\n${processes_completed[*]}"
		echo "Be sure to run this script an the root user also!"
	fi
}

## main - Make sure each param given is valid
function main() {
	cd $base

	# Run default setup if no flags are given
	if [[ ${#args[*]} == 0 ]]; then
		if [ "$(whoami)" == "root" ]; then
			echo "Running default installaion and setup..."
			echo -e "========================================\n"
		fi
		install_packages
		exit 0
	fi

	###  Run getopt  ###
	TEMP=$( getopt -o i::h:: -l "help::,install::" -n '$script' -- "$@" )
	eval set -- "$TEMP"
	while true; do
		case $1 in
			-h|--help)	# Send help
				shift
				display_usage
				;;
			-i|--install)	# Run install
				shift
				install_packages
				;;
			--) break ;; 
		esac
	done
}

## Main - Hello World
main $@
