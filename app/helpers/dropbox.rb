require 'dropbox_sdk'
require 'open-uri'
require_relative 'files.rb'

module DropboxHelper
  include FileHelper

  class DropboxUserClient
    attr_accessor :client

    def initialize(api_dir)
      @client = connect_dropbox(api_dir)
      @root   = "#{File.dirname(File.dirname(File.dirname(__FILE__)))}"
    end

    # Look in Dropbox for an existing file
    #=> Boolean
    def exists?(file)
      metadata = @client.search('/', file)
      if metadata.empty?
        false
      else
        metadata.each do |f|
          f.has_value?("/#{file}")
        end
      end
    end

    # Push a file to dropbox
    #=> String
    def dropbox_push(file)
      target = open(file)
      @client.put_file(File.basename(file), target)
    end

    # dropbox_pull - Pull file from dropbox and return the path it was restored
    #=> String
    def dropbox_pull(file)
      begin
        contents = @client.get_file("/#{file}")
        open("#{@root}/backups/#{file}", "w") { |f| f.puts contents }
        "#{@root}/backups/#{file}"
      rescue
        "Cannot download from Dropbox."
      end
    end

    # Destroy a file in Dropbox
    #=> String
    def dropbox_destroy(file)
      if exists?(file)
        @client.file_delete(file)
      else
        "#{file} does not exist in Dropbox!"
      end
    end

    # get_list - Pull metadata for files stored in the app directory
    #=> Array of Hash
    def get_list
      @client.metadata('/')["contents"]
    end

    # get_files - Pull file names from the app directory
    #=> Array of UserFile
    def get_files
      files, file_store = [], {}
      get_list.each { |meta| files << meta }
      files.each do |f|
        file = create_file(f)
        file_store.store(file[0], file[1])
      end
      file_store.each_value.sort_by { |f| f.modified[:mtime] }.reverse!
    end

    # create_file - takes an Array of files and returns UserFiles
    #=> Array of [path, UserFile]
    def create_file(file)
      begin
        user_file = FileHelper::UserFile.new("dropbox#{file["path"]}", true)
        parsed_mdate = parse_date(file["modified"])
        modified = {
          :mtime  => parsed_mdate,
          :date   => `echo #{parsed_mdate} | awk {'print $1'}`,
          :time   => `echo #{parsed_mdate} | awk {'print $2'}`
        }
        meta = {
          :fullpath => "dropbox#{file["path"]}",
          :basename => "#{file["path"][1..-1]}",
          :modified => modified
        }
        user_file.modified = modified
        user_file.metadata = meta
      rescue
        {}
      end
      ["#{user_file.fullpath}", user_file]
    end

    # parse_date - Convert the dropbox mdate to a Rubyesk mdate for sorting
    #=> String
    def parse_date(date)
      months = {
        "Jan" => 1, "Feb" => 2,
        "Mar" => 3, "Apr" => 4,
        "May" => 5, "Jun" => 6,
        "Jul" => 7, "Aug" => 8,
        "Sep" => 9, "Oct" => 9,
        "Nov" => 11, "Dec" => 12
      }
      begin
        year  = `echo #{date} | awk {'print $4'}`.chomp
        month = months[`echo #{date} | awk {'print $3'}`.chomp].to_s
        day   = `echo #{date} | awk {'print $2'}`.chomp
        time  = `echo #{date} | awk {'print $5'}`.chomp
        permission = `echo #{date} | awk {'print $6'}`.chomp
        return "#{year}-#{month}-#{day} #{time} #{permission}".chomp
      rescue
        raise "#{date} is not a string"
      end
    end

    private
      # Connect to Dropbox
      #=> Client || nil
      def connect_dropbox(api_dir)
        credentials = []
        # If any file is empty, insert 0, eventually returning nil
        Dir.foreach("#{api_dir}") do |file|
          next if file =~ /^\.{1,2}$/
          if File.zero?(file)
            credentials << 0
          else
            credentials << 1
          end
        end

        api_key     = open("#{api_dir}/.api_key").read
        app_secret  = open("#{api_dir}/.app_secret").read
        auth_token  = open("#{api_dir}/.auth_token").read

        unless credentials.include?(0)
          flow      = DropboxOAuth2FlowNoRedirect.new(api_key, app_secret)
          auth_url  = flow.start()
          return DropboxClient.new(auth_token)
        else
          return nil
        end
      end
  end # end class DropboxUserClient

end # end module BackupHelper