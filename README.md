## Backup Regime
#### Under Construction
This backup regime is a tool to leverage the versatility of the cloud with the user's privacy in mind. This application will spawn user-specified file, directory, and even full-system image backups that can be encrypted with a key only the user knows. These secure backups can then be synced with the user's personal cloud accounts after the accounts have been set up.

#### Installation
- Please keep in mind, this assumes you have [RVM] (Ruby 1.9+) installed and are running an apt-capable Linux.
- `sudo bin/setup.sh`
- `bin/setup.sh`

### Setup & Usage
Immediately after cloning and installing, run `./main` from the root directory of the project.

OR

Fire up a ruby server of your own and change the details in `app/app.rb`.

Next, setup a password that will be used to encrypt/decrypt your data upon compression. Note that this password will make it possible for you to decompress your data by hand at any time you like, via openssl.

#####Dropbox
To setup a dropbox account, navigate to the dropbox tab on the home page and follow the instructions thereafter. This will guide the user through the process of setting up a private API token that will be used by this program. Note that everything is kept in plain-text (blegh) so be careful with what you share.

##### Manual recovery
In the event that you lose this application or are unable to use it, follow these steps to decrypt and unpack any previously created backups.

`openssl aes-256-cbc -d -a < FILE.TAR.GZ.ENC > FILE.TAR.GZ` Keep the '< >'

`tar -zxpf FILE.TAR.GZ`

This should the file or directory that was backed-up, as well as a history.log that shows a history of what and where it was previous to backup.

#### Open Source For Life
Please fill out issues and contribute.

  - Your files
  - Your privacy
  - Your peace of mind

## License
[MIT]

[RVM]:rvm.io
[MIT]:http://opensource.org/licenses/MIT
